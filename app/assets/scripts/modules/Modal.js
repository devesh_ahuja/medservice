import $ from 'jquery';

class Modal{
    constructor(){
        this.openModalButton = $('.open-modal');
        this.modal = $('.modal');
        this.closeModalButton = $('.modal-close');
        this.events();
    }
    events(){
        this.openModalButton.click(this.openModal.bind(this)); // nind is used to the bind the 'this' obj of the callback function to whatever we want. Mostly it is used to bind the obj of the class
        // agar hum bind nahi karte aur this use karte callback funtion me to wo jis element ke wajah se event hua hai vo return karta 
        //
        
        this.closeModalButton.click(this.closeModal.bind(this));
        
        $(document).keyup(this.keyPressHandler.bind(this));
    }
    
    openModal(){
        this.modal.addClass('modal-is-visible');
        return false; //this will not perform its original work! same like preventDefault
    }
    closeModal(){
        this.modal.removeClass('modal-is-visible');
    }
    
    keyPressHandler(e){
        if(e.keyCode == 27){
            this.closeModal();
        }
    }
}

export default Modal;