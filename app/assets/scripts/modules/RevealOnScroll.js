import $ from 'jquery'; //since it is a node module there is no need to give the path for it

import waypoints from '../../../../node_modules/waypoints/lib/noframework.waypoints';//we have to give path here because it does not export uski file me export line nahi hai



class RevealOnScroll{
    constructor(els,offset = "60%"){
        this.itemsToReveal = els;
        this.offset = offset;
        
        
        this.hideInitially();
        this.createWayPoints();
    }
    hideInitially(){
        this.itemsToReveal.addClass('reveal-item');
    }
    
    createWayPoints(){
        
        var that = this;
        this.itemsToReveal.each(function(){
            let currentItem = this;
            new Waypoint({
                element : currentItem,
                handler: function(){
                $(currentItem).addClass('reveal-item-is-visible');
                },
                offset : that.offset
            });
        });
    }
}

export default RevealOnScroll;