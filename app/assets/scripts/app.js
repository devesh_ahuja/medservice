import $ from 'jquery';
import "lazysizes";
import "../styles/style.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from './modules/RevealOnScroll';
import SmoothScroll from './modules/SmoothScroll';
import activeLinks from './modules/ActiveLinks';
import Modal from './modules/Modal';


//Handles Moblie Menu /Headerr
let mobileMenu = new MobileMenu();

//Handles Reveal On Scroll
new RevealOnScroll($("#our-beginning"),"70%");
new RevealOnScroll($("#departments"));
new RevealOnScroll($("#counters"));
new RevealOnScroll($("#testimonials"));


//Adding smooth scroll functionality to our header links
new SmoothScroll();


//Making Link active on scroll
new activeLinks();

//Adding modal page
new Modal();

if(module.hot){
    module.hot.accept();
}

